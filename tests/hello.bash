#!/usr/bin/env bats

PROGRAM=hello
DIST_DIR=dist

@test "Test main program" {
    run ./$DIST_DIR/$PROGRAM
    [ "$output" == "Hello World" ]
}
