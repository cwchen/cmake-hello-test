' Set Program States
Dim Program : Program = "hello.exe"
Dim DistDir : DistDir = "dist"

Assert _
	Capture(DistDir & "\" & Program)(0) = "Hello World" & vbNewLine, _
	"Wrong value"

' Home-made assert for VBScript.
Sub Assert( boolExpr, strOnFail )
	If not boolExpr then
		Err.Raise vbObjectError + 99999, , strOnFail
	End If
End Sub

' Capture stdout and stderr from cmd
Function Capture(cmd)
	Set WshShell = WScript.CreateObject("WScript.Shell")

	Set output = WshShell.Exec("cmd.exe /c " & cmd)

	Dim arr(2)
	
	arr(0) = ""

	Do
		o = output.StdOut.ReadLine()
		arr(0) = arr(0) & o & vbNewLine
	Loop While Not output.Stdout.atEndOfStream
	
	arr(1) = ""
	
	Do
		e = output.StdErr.ReadLine()
		arr(1) = arr(1) & e & vbNewLine
	Loop While Not output.StdErr.atEndOfStream
	
	Capture = arr
End Function
