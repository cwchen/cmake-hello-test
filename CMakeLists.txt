cmake_minimum_required(VERSION 2.6)
project(Hello)

set(EXECUTABLE_OUTPUT_PATH dist)

include_directories(${Hello_SOURCE_DIR}/src)
include_directories(${Hello_SOURCE_DIR}/tests)

set(Hello_Sources ${Hello_SOURCE_DIR}/src/hello.c)

add_executable(hello ${Hello_Sources})

enable_testing()

if (WIN32)
    add_test(NAME hello COMMAND cscript ${Hello_SOURCE_DIR}/tests/hello.vbs)
else()
    add_test(NAME hello COMMAND bats ${Hello_SOURCE_DIR}/tests/hello.bash)
endif()